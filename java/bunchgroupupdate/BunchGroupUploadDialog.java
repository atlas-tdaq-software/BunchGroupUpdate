/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * BunchGroupUploadDialog.java
 *
 * Created on Nov 17, 2009, 11:27:39 AM
 */

package bunchgroupupdate;

import Igui.IguiLogger;
import java.awt.Color;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.DBConnections;
import triggerdb.Connections.InitInfo;
import triggerdb.Connections.UserMode;
import triggerdb.Entities.L1.L1BunchGroup;
import triggerdb.Entities.L1.L1BunchGroupSet;



/**
 *
 * @author Mark Owen <markowen@cern.ch>
 * @author Joerg Stelzer <stelzer@cern.ch>
 */
@SuppressWarnings("serial")
public class BunchGroupUploadDialog extends javax.swing.JDialog {

    // new bunch group set
    private final L1BunchGroupSet newBGS;

    // Vector of checkboxes
    private final List<JCheckBox> boxes = new ArrayList<>();
    
    // map to hold check boxes & their corresponding bunch groups
    //private final Map<JCheckBox,L1BunchGroup> boxmap_new = new HashMap<>();

    // Flag to use online naming scheme for bunch group sets (BunchGroups_date)
    //private final boolean useDateNaming = true;

    /** Creates new form BunchGroupUploadDialog
     * @param parent
     * @param theoldset
     * @param modal
     * @param thenewset */
    public BunchGroupUploadDialog(java.awt.Frame parent, 
            boolean modal, 
            L1BunchGroupSet theoldset, 
            L1BunchGroupSet thenewset) 
    {
        super(parent, modal);
        String msg = "BunchGroupUploader version " + 
                BunchGroupUploadDialog.class.getPackage().getImplementationVersion();
        IguiLogger.info(msg);

        initComponents();

        this.newBGS = thenewset;
        if(this.newBGS != null) {
            this.newBGS.set_partition(0);
        }
        
        boxes.add(bgCheckBox0);
        boxes.add(bgCheckBox1);
        boxes.add(bgCheckBox2);
        boxes.add(bgCheckBox3);
        boxes.add(bgCheckBox4);
        boxes.add(bgCheckBox5);
        boxes.add(bgCheckBox6);
        boxes.add(bgCheckBox7);
        boxes.add(bgCheckBox8);
        boxes.add(bgCheckBox9);
        boxes.add(bgCheckBox10);
        boxes.add(bgCheckBox11);
        boxes.add(bgCheckBox12);
        boxes.add(bgCheckBox13);
        boxes.add(bgCheckBox14);
        boxes.add(bgCheckBox15);
        if(this.newBGS!=null) {
            setCheckBoxLabels();
        }
        
        ToggleISSourceButton.setText("File-based BG (experts only)");
        
        
    }

    /** 
     * Set whether we are online or not
     * 
     * @param sw - true for online, false for offline.
     */
    public void setOnline(boolean sw) {
        //online = sw;
    }

    private void setCheckBoxLabels() {
        try {
            for (SimpleEntry<L1BunchGroup, String> entry : newBGS.getBunchGroups()) {
                L1BunchGroup bg = entry.getKey();
                String bgname = entry.getValue();
                
                int i = bg.get_internal_number();
                if ( i<0 || i>=boxes.size() ) {
                    continue;
                }
                JCheckBox box = boxes.get(i);
                box.setEnabled(true); 
                box.setSelected(true);
                box.setText(Integer.toString(i) + " - " + bgname + " [" + bg.get_Bunches().size() + "]");
//                boxmap_new.put(box, bg);
            }
        } catch(ArrayIndexOutOfBoundsException ex) {}
    }



    /**
     * SwingWorker to write the bg set to the db
     */
    private class BGWriterWorker extends SwingWorker<Void, Void> {

        private final L1BunchGroupSet bunchGroupSetForWriting;
        private final InitInfo originalInitInfo;
        private int newBGSK;

        BGWriterWorker(L1BunchGroupSet bgset, InitInfo originalInitInfo) {
            super();
            this.bunchGroupSetForWriting = bgset;
            this.originalInitInfo = originalInitInfo;
            this.newBGSK = -1; // new BGS key to be returned
        }

        @Override
        protected Void doInBackground() {

            if (bunchGroupSetForWriting != null) {
                
                // define the name of the bunch group set
                //if(useDateNaming) { // disabled, otherwise we have a new BGSK every day
                //DateFormat dateformat = new java.text.SimpleDateFormat("yyyy-MM-dd");
                String newname = "BunchGroupSetFromLHC" /*+ dateformat.format(new Date())*/;
                bunchGroupSetForWriting.set_name(newname);
                IguiLogger.info("Setting name of BunchGroupSet to: " + newname);
                //}

                // write the bunch group set
                try {
                    ArrayList<SimpleEntry<L1BunchGroup, String>> l1_bunchgroups = bunchGroupSetForWriting.getBunchGroups();
                    for(SimpleEntry<L1BunchGroup, String> bgentry : l1_bunchgroups ) {
                        L1BunchGroup bg = bgentry.getKey();
                        int bgid = bg.save();
                        IguiLogger.info("Saved BG " + bg.get_internal_number() + " with ID " + bgid + " " + bg.get_id() );
                        bg.set_id(bgid);
                    }
                                        
                    this.newBGSK = bunchGroupSetForWriting.save();
                    IguiLogger.info("Saved new bunchgroup set: " + this.newBGSK);
                } catch(Exception ex) {
                    IguiLogger.error("Error writing bunch groups: " + ex.getMessage());
                    ex.printStackTrace();
                }
                
            }            

            // disconnect from RW connection
            try {
                ConnectionManager.getInstance().disconnect();
                IguiLogger.info("After saving BGS, disconnected from read-write DB ok");
            } catch(Exception ex) {
                IguiLogger.warning("Problem disconnecting from the read-write trigger DB" + ex);
            }

            // connect to RO connection
            try {
                ConnectionManager.getInstance().connect(originalInitInfo);
                IguiLogger.info("Reconnected ok");
            } catch(Exception ex) {
                IguiLogger.error("Could not reconnect" + ex);
                JOptionPane.showMessageDialog(null, "Error reconnecting to the original read-only trigger DB.\n" + ex +"\nPlease RESTART THE IGUI\nand make an e-log entry", "Bunch Group Writing Error", JOptionPane.ERROR_MESSAGE);
            }

            return null;

        } //doInBackground

        @Override
        protected void done() {
            if(this.newBGSK>0) {
                busyLabel.setText("Done");
            } else {
                busyLabel.setForeground(Color.red);
                busyLabel.setText("Error");
            }
            JOptionPane.showMessageDialog(null, "Bunch Group Set Key: " + newBGSK);
            setVisible(false);            
            super.done();
        }


        
        
        
    }//class BGWriterWorker

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        bgCheckBox0 = new javax.swing.JCheckBox();
        bgCheckBox1 = new javax.swing.JCheckBox();
        bgCheckBox2 = new javax.swing.JCheckBox();
        bgCheckBox3 = new javax.swing.JCheckBox();
        bgCheckBox4 = new javax.swing.JCheckBox();
        bgCheckBox5 = new javax.swing.JCheckBox();
        bgCheckBox6 = new javax.swing.JCheckBox();
        bgCheckBox7 = new javax.swing.JCheckBox();
        bgCheckBox8 = new javax.swing.JCheckBox();
        bgCheckBox9 = new javax.swing.JCheckBox();
        bgCheckBox10 = new javax.swing.JCheckBox();
        bgCheckBox12 = new javax.swing.JCheckBox();
        bgCheckBox13 = new javax.swing.JCheckBox();
        bgCheckBox14 = new javax.swing.JCheckBox();
        bgCheckBox15 = new javax.swing.JCheckBox();
        bgCheckBox11 = new javax.swing.JCheckBox();
        busyLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        buttonPanel = new javax.swing.JPanel();
        writeButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        ToggleISSourceButton = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        bgCheckBox0.setText("0 - Not Used");
        bgCheckBox0.setDoubleBuffered(true);
        bgCheckBox0.setEnabled(false);

        bgCheckBox1.setText("1 - Not Used");
        bgCheckBox1.setEnabled(false);

        bgCheckBox2.setText("2 - Not Used");
        bgCheckBox2.setEnabled(false);

        bgCheckBox3.setText("3 - Not Used");
        bgCheckBox3.setEnabled(false);

        bgCheckBox4.setText("4 - Not Used");
        bgCheckBox4.setEnabled(false);

        bgCheckBox5.setText("5 - Not Used");
        bgCheckBox5.setEnabled(false);

        bgCheckBox6.setText("6 - Not Used");
        bgCheckBox6.setEnabled(false);

        bgCheckBox7.setText("7 - Not Used");
        bgCheckBox7.setEnabled(false);

        bgCheckBox8.setText("8 - Not Used");
        bgCheckBox8.setEnabled(false);

        bgCheckBox9.setText("9 - Not Used");
        bgCheckBox9.setEnabled(false);

        bgCheckBox10.setText("10 - Not Used");
        bgCheckBox10.setEnabled(false);

        bgCheckBox12.setText("13 - Not Used");
        bgCheckBox12.setEnabled(false);

        bgCheckBox13.setText("12 - Not Used");
        bgCheckBox13.setEnabled(false);

        bgCheckBox14.setText("15 - Not Used");
        bgCheckBox14.setEnabled(false);

        bgCheckBox15.setText("14 - Not Used");
        bgCheckBox15.setEnabled(false);

        bgCheckBox11.setText("11 - Not Used");
        bgCheckBox11.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bgCheckBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox0, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox4, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox5, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox6, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox7, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bgCheckBox9, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox8, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox14, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox15, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox12, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox13, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox11, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bgCheckBox10, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bgCheckBox0)
                    .addComponent(bgCheckBox8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bgCheckBox1)
                    .addComponent(bgCheckBox9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bgCheckBox2)
                    .addComponent(bgCheckBox10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bgCheckBox3)
                    .addComponent(bgCheckBox11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bgCheckBox4)
                    .addComponent(bgCheckBox13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bgCheckBox5)
                    .addComponent(bgCheckBox12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bgCheckBox6)
                    .addComponent(bgCheckBox15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bgCheckBox7)
                    .addComponent(bgCheckBox14))
                .addContainerGap())
        );

        busyLabel.setFont(new java.awt.Font("Luxi Sans", 1, 18)); // NOI18N
        busyLabel.setForeground(java.awt.Color.green);

        jLabel1.setFont(jLabel1.getFont().deriveFont(jLabel1.getFont().getStyle() | java.awt.Font.BOLD, jLabel1.getFont().getSize()+3));
        jLabel1.setForeground(new java.awt.Color(51, 0, 153));
        jLabel1.setText("Select bunch groups to update in the Trigger DB:");

        writeButton.setText("Write to DB");
        writeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                writeButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout buttonPanelLayout = new javax.swing.GroupLayout(buttonPanel);
        buttonPanel.setLayout(buttonPanelLayout);
        buttonPanelLayout.setHorizontalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(writeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        buttonPanelLayout.setVerticalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, buttonPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(writeButton, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                    .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        ToggleISSourceButton.setText("LHC based (always when LHC is on)");
        ToggleISSourceButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ToggleISSourceButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(busyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(ToggleISSourceButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(401, 401, 401)))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(busyLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(ToggleISSourceButton)
                .addGap(25, 25, 25))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void writeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_writeButtonActionPerformed


        // All boxes should be check since the BGS should contain all BG
        boolean hasUnselectedBox=false;
        for(JCheckBox box : boxes) {
            if(!box.isSelected()) { hasUnselectedBox = true; }
        }
        if( hasUnselectedBox ) {
            JOptionPane.showMessageDialog(null,
                    "Not all bunch groups are defined in this request.\nPlease inform the trigger expert before proceeding!",
                    "Bunch Group Incomplete", JOptionPane.WARNING_MESSAGE);
        }

        DBConnections.setOnline(true);
        InitInfo initInfo = DBConnections.createAtonrWriter();
        initInfo.setSystemUsername("BunchGroupWriter");
        initInfo.setgoodInfo(true);
        
        if ( !initInfo.getgoodInfo() || initInfo.getUserMode()==UserMode.EXIT) {
            IguiLogger.info(""+initInfo);
            initInfo.setUserMode(UserMode.EXIT);
            IguiLogger.error("Login not accepted\n" + initInfo);
            return;
        } else {
            IguiLogger.info("Login accepted\n" + initInfo );
        }

        // safe original InitInfo
        InitInfo originalInitInfo = ConnectionManager.getInstance().getInitInfo();

        // disconnect
        try {
            ConnectionManager.getInstance().disconnect();
            IguiLogger.info("Disconnected from read-only DB ok");
        } catch(Exception ex) {
            IguiLogger.warning("Could not disconnect from read-only DB");
        }

        // reconnect
        try {
            ConnectionManager.getInstance().connect(initInfo);
            IguiLogger.info("Connected to RW DB ok");
        } catch(Exception ex) {
            IguiLogger.error("Could not connect to read-write DB");
            JOptionPane.showMessageDialog(null, "Error connecting to the read-write trigger DB" + ex, "Bunch Group Writing Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        // Now we are connected and can write things.

        // disable all buttons & checkboxes
        for(JCheckBox box : boxes) { box.setEnabled(false); }
        writeButton.setEnabled(false);
        cancelButton.setEnabled(false);
        // panel is busy now
        busyLabel.setText("Writing to DB ...");

        // Do the actual DB writing and closing in a different thread
        BGWriterWorker bgww = new BGWriterWorker(newBGS, originalInitInfo);
        bgww.execute();

    }//GEN-LAST:event_writeButtonActionPerformed

    private void ToggleISSourceButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ToggleISSourceButtonActionPerformed
        // TODO add your handling code here:
        if(ToggleISSourceButton.isSelected()) {
            ToggleISSourceButton.setText("File-based (experts only)");
            
        } else {
            ToggleISSourceButton.setText("LHC based (always when LHC is on)");
        }
    }//GEN-LAST:event_ToggleISSourceButtonActionPerformed


    public static void main(String[] args) {
        new BunchGroupUploadDialog(null,true,null,null).setVisible(true);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton ToggleISSourceButton;
    private javax.swing.JCheckBox bgCheckBox0;
    private javax.swing.JCheckBox bgCheckBox1;
    private javax.swing.JCheckBox bgCheckBox10;
    private javax.swing.JCheckBox bgCheckBox11;
    private javax.swing.JCheckBox bgCheckBox12;
    private javax.swing.JCheckBox bgCheckBox13;
    private javax.swing.JCheckBox bgCheckBox14;
    private javax.swing.JCheckBox bgCheckBox15;
    private javax.swing.JCheckBox bgCheckBox2;
    private javax.swing.JCheckBox bgCheckBox3;
    private javax.swing.JCheckBox bgCheckBox4;
    private javax.swing.JCheckBox bgCheckBox5;
    private javax.swing.JCheckBox bgCheckBox6;
    private javax.swing.JCheckBox bgCheckBox7;
    private javax.swing.JCheckBox bgCheckBox8;
    private javax.swing.JCheckBox bgCheckBox9;
    private javax.swing.JLabel busyLabel;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton writeButton;
    // End of variables declaration//GEN-END:variables

}
