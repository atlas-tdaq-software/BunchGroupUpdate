package bunchgroupupdate;

import TTCInfo.TrigConfL1BunchGroups;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.logging.Logger;
import triggerdb.Entities.L1.L1Bunch;
import triggerdb.Entities.L1.L1BunchGroup;
import triggerdb.Entities.L1.L1BunchGroupSet;


class BGHelper {

    
    /**
     * This function uses the TrigConfL1BunchGroups is Info object to extract
     * the recommended bunch groups.
     * @param isbgset - is bunch group set
     * @return triggerdb L1BunchGroupSet
     */
    static L1BunchGroupSet createNewBunchGroupSet(final TrigConfL1BunchGroups isbgset) {
        L1BunchGroupSet newset = new L1BunchGroupSet();
        newset.set_name(isbgset.BunchGroupSet);

        LOG.fine("Creating new BunchGroupSet from IS object with");
        
        ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> bunchGroups = newset.getBunchGroups();
        bunchGroups.clear();
        
        
        // Add each of the 8 bunch groups
        L1BunchGroup[] bg = new L1BunchGroup[16];
        for(int i=0; i<16; i++) {
            bg[i] = new L1BunchGroup();
            bg[i].set_internal_number(i);
        }
        
        // set the names
        String[] bgnames = new String[16];
        bgnames[0] = isbgset.Name_Group_0;
        bgnames[1] = isbgset.Name_Group_1;
        bgnames[2] = isbgset.Name_Group_2;
        bgnames[3] = isbgset.Name_Group_3;
        bgnames[4] = isbgset.Name_Group_4;
        bgnames[5] = isbgset.Name_Group_5;
        bgnames[6] = isbgset.Name_Group_6;
        bgnames[7] = isbgset.Name_Group_7;
        bgnames[8] = isbgset.Name_Group_8;
        bgnames[9] = isbgset.Name_Group_9;
        bgnames[10] = isbgset.Name_Group_10;
        bgnames[11] = isbgset.Name_Group_11;
        bgnames[12] = isbgset.Name_Group_12;
        bgnames[13] = isbgset.Name_Group_13;
        bgnames[14] = isbgset.Name_Group_14;
        bgnames[15] = isbgset.Name_Group_15;
        
        
        fillArray(bg[0], isbgset.BCID_Group_0);
        fillArray(bg[1], isbgset.BCID_Group_1);
        fillArray(bg[2], isbgset.BCID_Group_2);
        fillArray(bg[3], isbgset.BCID_Group_3);
        fillArray(bg[4], isbgset.BCID_Group_4);
        fillArray(bg[5], isbgset.BCID_Group_5);
        fillArray(bg[6], isbgset.BCID_Group_6);
        fillArray(bg[7], isbgset.BCID_Group_7);
        fillArray(bg[8], isbgset.BCID_Group_8);
        fillArray(bg[9], isbgset.BCID_Group_9);
        fillArray(bg[10], isbgset.BCID_Group_10);
        fillArray(bg[11], isbgset.BCID_Group_11);
        fillArray(bg[12], isbgset.BCID_Group_12);
        fillArray(bg[13], isbgset.BCID_Group_13);
        fillArray(bg[14], isbgset.BCID_Group_14);
        fillArray(bg[15], isbgset.BCID_Group_15);
        
        // fill the bunchgroups
        for(int i=0; i<16; ++i) {
            newset.getBunchGroups().add(new AbstractMap.SimpleEntry<>(bg[i],bgnames[i]) );
        }
        return newset;
    }
    private static final Logger LOG = Logger.getLogger(BGHelper.class.getName());

    static void fillArray(L1BunchGroup bg, int[] BCID_Group) {
        for(int bunchnum : BCID_Group) {
            L1Bunch bunch = new L1Bunch();
            bunch.set_bunch_number(bunchnum);
            bg.get_Bunches().add(bunch);
        }
    }



}
