/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bunchgroupupdate;

import java.util.AbstractMap.SimpleEntry;
import java.util.Iterator;
import triggerdb.Entities.L1.L1Bunch;
import triggerdb.Entities.L1.L1BunchGroup;
import triggerdb.Entities.L1.L1BunchGroupSet;

/**
 *
 * @author markowen
 */
public class BunchGroupUpdateTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        BunchGroupUpdateTest test = new BunchGroupUpdateTest();

        if (args.length > 0) {
            if (args[0].contentEquals("-standalone") || args[0].contentEquals("-s")) {
                test.StandAloneTest();
            } else {
                if (args[0].contentEquals("-is")) {
                    test.ISTest();
                } else {
                    System.out.println("Please provide either -standalone / -s for standalone or -is for IS.");
                }
            }
        } else {
            test.ISTest();
        }
        //test.StandAloneTest();

        System.exit(0);
    }

    /**
     * Test the system with IS input. Starts with a dummy BG set, fetches
     * recommended set from IS & lets user write to a trigger DB.
     */
    public void ISTest() {
        /// subscribe to IS
        BunchGroupISListener is_list = new BunchGroupISListener("initialL1CT");
        is_list.subscribe();
        /// get recommended BGS from IS
        System.out.println("Getting BGS");
        L1BunchGroupSet newbgset = is_list.getRecommendedBunchGroups();
        System.out.println("Got BGS " + newbgset.get_name());
        for (Iterator<SimpleEntry<L1BunchGroup, String>> it = newbgset.getBunchGroups().iterator(); it.hasNext();) {
            SimpleEntry<L1BunchGroup, String> bg = it.next();
            System.out.println("Bunch Group " + bg.getValue());
            for (L1Bunch bunch : bg.getKey().get_Bunches()) {
                System.out.print(bunch.get_bunch_number() + ", ");
            }
            System.out.println("");
        }

        /// make dummy bunch group set
        L1BunchGroupSet oldset = new L1BunchGroupSet();
        L1BunchGroup o_bg0 = new L1BunchGroup();
        L1Bunch o_b0 = new L1Bunch();
        o_b0.set_bunch_number(10);
        o_bg0.set_name("BG0");
        o_bg0.set_internal_number(0);
        o_bg0.get_Bunches().add(o_b0);
        oldset.getBunchGroups().add( new SimpleEntry<>( o_bg0,o_bg0.get_name() ) );

        /// launch dialog
        BunchGroupUploadDialog dg = new BunchGroupUploadDialog(null, true, oldset, newbgset);
        final String host = java.lang.System.getenv("HOSTNAME");
        if (host.contains("pc-atlas-cr")) {
            dg.setOnline(true);
        } else {
            dg.setOnline(false);
        }
        dg.setVisible(true);

        is_list.unsubscribe();
    }

    /**
     * Test the mechanism with dummy bunch groups.
     */
    public void StandAloneTest() {
        /// make dummy bunch group sets
        L1BunchGroupSet oldset = new L1BunchGroupSet();
        L1BunchGroup o_bg0 = new L1BunchGroup();
        L1Bunch o_b0 = new L1Bunch();
        o_b0.set_bunch_number(10);
        o_bg0.set_name("BG0");
        o_bg0.set_internal_number(0);
        o_bg0.get_Bunches().add(o_b0);
        oldset.getBunchGroups().add(new SimpleEntry<>(o_bg0,o_bg0.get_name()));
        L1BunchGroup o_bg1 = new L1BunchGroup();
        L1Bunch o_b1 = new L1Bunch();
        o_b1.set_bunch_number(12);
        o_bg1.set_name("BG1");
        o_bg1.set_internal_number(1);
        o_bg1.get_Bunches().add(o_b1);
        oldset.getBunchGroups().add(new SimpleEntry<>(o_bg1,o_bg1.get_name()));
        L1BunchGroup o_bg2 = new L1BunchGroup();
        L1Bunch o_b2 = new L1Bunch();
        o_b2.set_bunch_number(400);
        o_bg2.set_name("BG2");
        o_bg2.set_internal_number(2);
        o_bg2.get_Bunches().add(o_b2);
        oldset.getBunchGroups().add(new SimpleEntry<>(o_bg2,o_bg2.get_name()));
        L1BunchGroup o_bg3 = new L1BunchGroup();
        L1Bunch o_b3 = new L1Bunch();
        o_b3.set_bunch_number(299);
        o_bg3.set_name("BG3");
        o_bg3.set_internal_number(3);
        o_bg3.get_Bunches().add(o_b3);
        oldset.getBunchGroups().add(new SimpleEntry<>(o_bg3,o_bg3.get_name()));

        L1BunchGroupSet newset = new L1BunchGroupSet();
        L1BunchGroup n_bg0 = new L1BunchGroup();
        L1Bunch n_b0 = new L1Bunch();
        n_b0.set_bunch_number(20);
        n_bg0.set_name("BG0");
        n_bg0.set_internal_number(0);
        n_bg0.get_Bunches().add(n_b0);
        newset.getBunchGroups().add(new SimpleEntry<>(n_bg0,n_bg0.get_name()));
        L1BunchGroup n_bg1 = new L1BunchGroup();
        L1Bunch n_b1 = new L1Bunch();
        n_b1.set_bunch_number(22);
        n_bg1.set_name("BG1");
        n_bg1.set_internal_number(1);
        n_bg1.get_Bunches().add(n_b1);
        newset.getBunchGroups().add(new SimpleEntry<>(n_bg1,n_bg1.get_name()));
        L1BunchGroup n_bg2 = new L1BunchGroup();
        L1Bunch n_b2 = new L1Bunch();
        n_b2.set_bunch_number(620);
        n_bg2.set_name("BG2");
        n_bg2.set_internal_number(2);
        n_bg2.get_Bunches().add(n_b2);
        newset.getBunchGroups().add(new SimpleEntry<>(n_bg2,n_bg2.get_name()));
        L1BunchGroup n_bg3 = new L1BunchGroup();
        L1Bunch n_b3 = new L1Bunch();
        n_b3.set_bunch_number(630);
        n_bg3.set_name("BG3");
        n_bg3.set_internal_number(3);
        n_bg3.get_Bunches().add(n_b3);
        newset.getBunchGroups().add(new SimpleEntry<>(n_bg3,n_bg3.get_name()));

        BunchGroupUploadDialog dialog = new BunchGroupUploadDialog(new javax.swing.JFrame(), true, oldset, newset);
        final String host = java.lang.System.getenv("HOSTNAME");
        if (host.contains("pc-atlas-cr")) {
            dialog.setOnline(true);
        } else {
            dialog.setOnline(false);
        }
        dialog.setVisible(true);

        while (dialog.isVisible()) {
        }

        int res = javax.swing.JOptionPane.showConfirmDialog(null, "Click to finish test");
    }
}
