package bunchgroupupdate;

import TTCInfo.TrigConfL1BunchGroups;
import TTCInfo.BGStatus;
import is.InfoNotCompatibleException;
import is.InfoNotFoundException;
import is.Repository;
import is.RepositoryNotFoundException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Arrays;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.DBConnections;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.L1.L1BunchGroup;
import triggerdb.Entities.L1.L1BunchGroupSet;

/**
 *
 * @author stelzer
 */
public class BunchGroupSetUploadCmd {

    private static void printHelp() {
        System.out.println("Options");
        System.out.println("    -h               this help");
        System.out.println("    -n <bgsname>     name of the bunchgroup set (no /)");
    }

    /** IS repository */
    private final Repository isRepository;

    /** partition name */
    private final String partitionName = "initialL1CT";

    /** IS server name */
    private final String serverName = "Monitoring";

    /** IS object name */
    private final String objectName = "BunchGrouperApp_FS/BunchGroups_FS";
    private final String statusName = "BunchGrouperApp_FS/BunchGroups_FS_Status";
    
    private L1BunchGroupSet bgs = null;

    private String bgsName = "BunchGroupSetFromLHC" /*+ dateformat.format(new Date())*/;

    public static void main(String[] args) {
        String last = "";
        String bgsName = "BunchGroupSetFromLHC";
        for(final String arg : args) {
            if(arg.equals("-h")) {
                printHelp();
                return;
            }
            if(last.equals("-n")) {
                bgsName = arg;
                int slashIdx = bgsName.indexOf('/');
                if(slashIdx>=0) {
                    bgsName = bgsName.substring(slashIdx);
                }
            }
            last = arg;
        }
        System.out.println("Will upload with name " + bgsName);

        BunchGroupSetUploadCmd bgsLoader = new BunchGroupSetUploadCmd(bgsName);
        
        bgsLoader.getRecommendedBunchGroupSetFromIS();
        
        bgsLoader.print();

        if(bgsLoader.askConfirmation("Would you like to upload this to the TriggerDB [y|N]? : ")) {
            System.out.println("Going to upload" );
            if(bgsLoader.connectTriggerDB()) {
                bgsLoader.upload();
                bgsLoader.disconnectTriggerDB();
            }
        } else {
            System.out.println("Cancel process" );
        }
    }

    public BunchGroupSetUploadCmd(final String bgsName) {
        System.out.println("Connecting to IS repository of partition " + partitionName );
        this.bgsName = bgsName;
        this.isRepository = new Repository(new ipc.Partition(partitionName));
    }

    private void getRecommendedBunchGroupSetFromIS() {
        try{

            TrigConfL1BunchGroups isinfo_init = new TrigConfL1BunchGroups();
            isRepository.getValue(serverName + "." + objectName,isinfo_init);
           
	    // get BG from IS untill a different one is found
	    TrigConfL1BunchGroups isinfo = new TrigConfL1BunchGroups(); 
	    Boolean is_updated = false;
	    int counter = 0;
	    while(!is_updated) {
		isRepository.getValue(serverName + "." + objectName,isinfo);
		
		int[] bg1 = isinfo.BCID_Group_1;
	      	       
		if(!Arrays.equals(isinfo.BCID_Group_0,isinfo_init.BCID_Group_0)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_1,isinfo_init.BCID_Group_1)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_2,isinfo_init.BCID_Group_2)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_3,isinfo_init.BCID_Group_3)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_4,isinfo_init.BCID_Group_4)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_5,isinfo_init.BCID_Group_5)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_6,isinfo_init.BCID_Group_6)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_7,isinfo_init.BCID_Group_7)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_8,isinfo_init.BCID_Group_8)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_9,isinfo_init.BCID_Group_9)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_10,isinfo_init.BCID_Group_10)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_11,isinfo_init.BCID_Group_11)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_12,isinfo_init.BCID_Group_12)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_13,isinfo_init.BCID_Group_13)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_14,isinfo_init.BCID_Group_14)) is_updated = true;
		else if(!Arrays.equals(isinfo.BCID_Group_15,isinfo_init.BCID_Group_15)) is_updated = true;
		else try{Thread.sleep(1000);}catch(InterruptedException e){System.out.println(e);};
		
		counter += 1;
		if(counter==80){
		    System.out.println("WARNING TIMEOUT: IS not updated after 80 seconds. Please check carefully the BG set below:");
		    System.out.println("  -if the BG set does NOT match the input pattern: do NOT upload this BGK to the triggerDB and try again");
		    System.out.println("  -if the BG set does match the input pattern: you can upload this BGK to the triggerDB");
		    is_updated = true;
		}
	    }
	    System.out.println("Getting TrigConfL1BunchGroups from " + serverName + "." + objectName);
	    bgs = BGHelper.createNewBunchGroupSet(isinfo);

        } catch (RepositoryNotFoundException | InfoNotFoundException | InfoNotCompatibleException ex) {
            Logger.getLogger(BunchGroupSetUploadCmd.class.getName()).log(Level.SEVERE, "Can not get recommended bunchgroup set from IS (" + serverName + "." + objectName + ")", ex);
        }
        
        // remove _FS from BG name
        ArrayList<SimpleEntry<L1BunchGroup, String>> l1_bunchgroups = this.bgs.getBunchGroups();
        for(SimpleEntry<L1BunchGroup, String> bgentry : l1_bunchgroups ) {
            String bgName = bgentry.getValue();
            if( bgName.endsWith("_FS") ) {
                bgName = bgName.substring(0, bgName.length()-3);
                bgentry.setValue(bgName);
            }
        }
        System.out.println(bgs.toString());
    }

    private void print() {
        for (SimpleEntry<L1BunchGroup, String> entry : this.bgs.getBunchGroups()) {
            L1BunchGroup bg = entry.getKey();
            String bgname = entry.getValue();
            int i = bg.get_internal_number();
            System.out.println(i + " - " + bgname + " [" + bg.get_Bunches().size() + "]");
        }
    }
    
    private boolean askConfirmation(final String prompt) {
  
        Scanner reader = new Scanner(System.in);
        System.out.print(prompt);
        char c = reader.next().charAt(0);
        boolean isYes = (c=='Y' || c=='y');
        //System.out.println("answer was " + (isYes?"YES":"NO"));
        return isYes;
    }

    private void upload() {
        if(this.bgs == null) {
            return;
        }
    
        // define the name of the bunch group set
        //if(useDateNaming) { // disabled, otherwise we have a new BGSK every day
        //DateFormat dateformat = new java.text.SimpleDateFormat("yyyy-MM-dd");
        this.bgs.set_name(this.bgsName);
        System.out.println("Setting name of BunchGroupSet to: " + this.bgsName);
        //}

        // write the bunch group set
        try {
            ArrayList<SimpleEntry<L1BunchGroup, String>> l1_bunchgroups = this.bgs.getBunchGroups();
            for(SimpleEntry<L1BunchGroup, String> bgentry : l1_bunchgroups ) {
                L1BunchGroup bg = bgentry.getKey();
                int bgid = bg.save();
                System.out.println("Saved BG " + bg.get_internal_number() + " with ID " + bg.get_id() );
                bg.set_id(bgid);
            }

            int bgsKey = this.bgs.save();
            System.out.println("=========================\n");
            System.out.println("Bunchgroup set key : " + bgsKey);
            System.out.println("\n=========================");
        } catch(Exception ex) {
            System.err.println("Error writing bunch groups: " + ex.getMessage());
            ex.printStackTrace();
        }

    }            

    private boolean connectTriggerDB() {
        DBConnections.setOnline(true);
        InitInfo initInfo = DBConnections.createAtonrWriter();
        initInfo.setSystemUsername("BunchGroupWriter");
        initInfo.setgoodInfo(true);
        
        try {
            ConnectionManager.getInstance().connect(initInfo);
            System.out.println("Connected to RW DB ok");
        } catch(Exception ex) {
            System.err.println("Could not connect to read-write DB");
            return false;
        }
        return true;
    }
    
    private void disconnectTriggerDB() {
        try {
            ConnectionManager.getInstance().disconnect();
            System.out.println("Disconnected from TriggerDB");
        } catch(Exception ex) {
            System.out.println("Problem disconnecting from the read-write trigger DB" + ex);
        }
    }
}
