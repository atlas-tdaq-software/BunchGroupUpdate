/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bunchgroupupdate;

import Igui.IguiLogger;
import TTCInfo.TrigConfL1BunchGroups;
import is.AlreadySubscribedException;
import is.Criteria;
import is.InfoEvent;
import is.InfoListener;
import is.InfoNotCompatibleException;
import is.InfoNotFoundException;
import is.InvalidCriteriaException;
import is.Repository;
import is.RepositoryNotFoundException;
import is.SubscriptionNotFoundException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import triggerdb.Entities.L1.L1Bunch;
import triggerdb.Entities.L1.L1BunchGroup;
import triggerdb.Entities.L1.L1BunchGroupSet;

/**
 *
 * @author markowen
 */
public class BunchGroupISListener implements InfoListener {

    /** IS repository */
    private final Repository isRepository;

    /** partition name */
    private final String partitionName;

    /** Are we subscribed? */
    private boolean subscribed;

    /** IS server name */
    private final String serverName = "Monitoring";

    /** IS object name */
    private final String objectName = "BunchGrouperApp/BunchGroups";

    public BunchGroupISListener(String partname) {
        partitionName = partname;
        subscribed=false;
        // connect to IS repository
        isRepository = new Repository(new ipc.Partition(partitionName));
    }

    /**
     * Return a string to show the partition name & serve.object used to contact IS.
     * @return
     */
    public String getConnString() {
        return "Partition: " + partitionName + ", Object: " + serverName + "." + objectName;
    }

    /**
     * This function contacts the IS server & gets the recommended bunch
     * groups from IS.
     * Use the parseNewBunchGroups function.
     * @return The new recommended bunch group set.
     */
    public L1BunchGroupSet getRecommendedBunchGroups() {
        try{
            TrigConfL1BunchGroups isinfo = new TrigConfL1BunchGroups();
            isRepository.getValue(serverName + "." + objectName,isinfo);
            IguiLogger.info("Getting TrigConfL1BunchGroups from " + serverName + "." + objectName);
            return createNewBunchGroupSet(isinfo);
        } catch (RepositoryNotFoundException ex) {
            System.err.println("RepositoryNotFoundException for " + serverName + "." + objectName);
            return null;
        } catch (InfoNotFoundException ex) {
            System.err.println("InfoNotFoundException for " + serverName + "." + objectName);
            return null;
        } catch (InfoNotCompatibleException ex) {
            Logger.getLogger(BunchGroupISListener.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * This function uses the TrigConfL1BunchGroups is Info object to extract
     * the recommended bunch groups.
     * @param isbgset - is bunch group set
     * @return triggerdb L1BunchGroupSet
     */
    private L1BunchGroupSet createNewBunchGroupSet(final TrigConfL1BunchGroups isbgset) {
        L1BunchGroupSet newset = new L1BunchGroupSet();
        newset.set_name(isbgset.BunchGroupSet);

        IguiLogger.info("Creating new BunchGroupSet from IS object with");
        
        ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> bunchGroups = newset.getBunchGroups();
        bunchGroups.clear();
        
        
        // Add each of the 8 bunch groups
        L1BunchGroup[] bg = new L1BunchGroup[16];
        for(int i=0; i<16; i++) {
            bg[i] = new L1BunchGroup();
            bg[i].set_internal_number(i);
        }
        
        // set the names
        String[] bgnames = new String[16];
        bgnames[0] = isbgset.Name_Group_0;
        bgnames[1] = isbgset.Name_Group_1;
        bgnames[2] = isbgset.Name_Group_2;
        bgnames[3] = isbgset.Name_Group_3;
        bgnames[4] = isbgset.Name_Group_4;
        bgnames[5] = isbgset.Name_Group_5;
        bgnames[6] = isbgset.Name_Group_6;
        bgnames[7] = isbgset.Name_Group_7;
        bgnames[8] = isbgset.Name_Group_8;
        bgnames[9] = isbgset.Name_Group_9;
        bgnames[10] = isbgset.Name_Group_10;
        bgnames[11] = isbgset.Name_Group_11;
        bgnames[12] = isbgset.Name_Group_12;
        bgnames[13] = isbgset.Name_Group_13;
        bgnames[14] = isbgset.Name_Group_14;
        bgnames[15] = isbgset.Name_Group_15;
        
        
        fillArray(bg[0], isbgset.BCID_Group_0);
        fillArray(bg[1], isbgset.BCID_Group_1);
        fillArray(bg[2], isbgset.BCID_Group_2);
        fillArray(bg[3], isbgset.BCID_Group_3);
        fillArray(bg[4], isbgset.BCID_Group_4);
        fillArray(bg[5], isbgset.BCID_Group_5);
        fillArray(bg[6], isbgset.BCID_Group_6);
        fillArray(bg[7], isbgset.BCID_Group_7);
        fillArray(bg[8], isbgset.BCID_Group_8);
        fillArray(bg[9], isbgset.BCID_Group_9);
        fillArray(bg[10], isbgset.BCID_Group_10);
        fillArray(bg[11], isbgset.BCID_Group_11);
        fillArray(bg[12], isbgset.BCID_Group_12);
        fillArray(bg[13], isbgset.BCID_Group_13);
        fillArray(bg[14], isbgset.BCID_Group_14);
        fillArray(bg[15], isbgset.BCID_Group_15);
        
        // fill the bunchgroups
        for(int i=0; i<16; ++i) {
            newset.getBunchGroups().add(new AbstractMap.SimpleEntry<>(bg[i],bgnames[i]) );
        }
        IguiLogger.info(newset.toString());
        return newset;
    }

    private void fillArray(L1BunchGroup bg, int[] BCID_Group) {
        for(int bunchnum : BCID_Group) {
            L1Bunch bunch = new L1Bunch();
            bunch.set_bunch_number(bunchnum);
            bg.get_Bunches().add(bunch);
        }
    }

    
    /**
     * Subscribe to the IS server
     */
    public void subscribe() {
        if(!subscribed) {
            try {
                isRepository.subscribe( serverName, new is.Criteria(Pattern.compile(objectName)), this);
                subscribed = true;
            } 
            catch(RepositoryNotFoundException | InvalidCriteriaException ex) {
                subscribed = false;
            } catch (AlreadySubscribedException ex) {
            }
        }
    }//subscribe()

    /**
     * Subscribe to the IS server
     */
    public void unsubscribe() {
        if(subscribed) {
            try {
                isRepository.unsubscribe( serverName, new Criteria(Pattern.compile(objectName)) );
                subscribed = false;
            } catch( RepositoryNotFoundException | SubscriptionNotFoundException ex) {

            }
        }
    }//unsubscribe()

    ////// -----------------------------------------------------
    ///////// Methods below are from InfoListener interface
    //////
    ////// Currently we don't do anything when the info changes.
    ////// -----------------------------------------------------

    @Override
    public void infoCreated(InfoEvent infoevent) {

        /*TrigConfL1BunchGroups isobject = new TrigConfL1BunchGroups();
        try {
            infoevent.getValue(isobject);
            // should now do something with the is info
        } catch (is.InfoNotCompatibleException ex) {
            System.err.println("WARNING: Unexpected info type");
        }*/

    }

    @Override
    public void infoUpdated(InfoEvent infoevent) {}

    @Override
    public void infoDeleted(InfoEvent infoevent) {}

}
